import pandas as pd
import spacy
import numpy as np

nlp=spacy.load("en_core_web_md")

data = pd.read_csv("../../data/train.csv")['text'].values

with open("COVID_corpus.txt","w",encoding="utf-8") as f:
    for i in range(len(data)):
        document=nlp(str(data[i]))
        number=0
        for sent in document.sents:
            number+=1
            f.write(str(sent)+"\n")
        f.write("\n")
